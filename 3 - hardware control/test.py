import RPi.GPIO as GPIO
import time
from multiprocessing import Process
import _thread

def input_thread(a_list):
	input()
	a_list.append(True)

def do_stuff():
	a_list = []
	_thread.start_new_thread(input_thread, (a_list,))
	while not a_list:
		stuff()

outvrb = 11
clock = 13
clear = 15
latch = 16

speedShifts = 0.00005

output = [0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0]

demoMatrix = [[0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0],[1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0],[1,0,0,0,1,1,1,0,0,0,1,1,1,0,0,0],[1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0],[0,1,0,1,0,1,0,1,1,0,1,0,1,0,1,0],[1,0,1,0,1,0,1,0,0,1,0,1,0,1,0,1]]


def clockProcess():
	while(True):
		#print ('CLOCK')
		GPIO.output(clock,GPIO.HIGH)
		time.sleep(speedShifts)
		GPIO.output(clock,GPIO.LOW)
		time.sleep(speedShifts)


def shift(input):
	if(input == 1):
		GPIO.output(outvrb,GPIO.HIGH)
	elif(input == 0):
		GPIO.output(outvrb,GPIO.LOW)
	else:
		return

def setLatch(input):
	if(input == 1):
		GPIO.output(latch,GPIO.HIGH)
		print('latch on')
	elif(input == 0):
		GPIO.output(latch,GPIO.LOW)
		print('latch off')
	else:
		return

		
def pulseClock():
	print ('pulse')
	GPIO.output(clock,GPIO.HIGH)
	time.sleep(speedShifts)
	GPIO.output(clock,GPIO.LOW)
	time.sleep(speedShifts)
	
def setToOutput():
	setLatch(0)
	global output
	for i in range(16,0, -1):
		shift(output[i-1])
		pulseClock()
	setLatch(1)

def demoMode():
	global output
	global demoMatrix	
	
	a_list = []
	_thread.start_new_thread(input_thread, (a_list,))
	while not a_list:
		for i in range(len(demoMatrix)):
			output = demoMatrix[i]
			print(output)
			setToOutput()
			time.sleep(0.05)
		
def flickerMode():
	global output
	
	for i in range(201):
		a = i%2
		if(a == 1):
			output = [0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1]
		else:
			output = [1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0]		
		print(output)
		setToOutput()
		time.sleep(0.05)
		
def testSingle(input):
	global output
	if(input > len(output)):
		return
	elif(input < 0):
		return
	else:
		output = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		output[input] = 1
		setToOutput()
			
def testWave():
	global output
	a_list = []
	_thread.start_new_thread(input_thread, (a_list,))
	while not a_list:
		for i in range (len(output)):
			testSingle(i)
			time.sleep(0.25)
		
def main():

	print ('pin outvrb: ', outvrb, 'h for high, l for low')
	print ('pin clock: ', clock, 'p on manual, a to set automatic, cant set to manual after')
	print ('pin clear: ', clear, 'c')
	GPIO.setmode(GPIO.BOARD) 
	GPIO.setup(outvrb, GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(clock, GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(clear, GPIO.OUT, initial=GPIO.HIGH)
	GPIO.setup(latch, GPIO.OUT, initial=GPIO.HIGH)
	
	GPIO.output(clear,GPIO.LOW)
	time.sleep(speedShifts)
	GPIO.output(clear,GPIO.HIGH)
	time.sleep(speedShifts)
	
	aux = ''
	
	clockP = Process(target=clockProcess)
	clockP.daemon = True
	auxclockP = False
	
	global output
	
	print(output)
	
	
	setToOutput()
			
	while(aux != 'q'):	
		aux = input('Enter your input:') 
		if (aux == 'h'):
			shift(1)
			pulseClock()
			output.pop(len(output)-1)
			output.insert(0,1)
			print(output)
		if (aux == 'l'):
			shift(0)
			pulseClock()
			output.pop(len(output)-1)
			output.insert(0,0)
			print(output)
		if (aux == 'c'):
			print ('clear')
			output = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			GPIO.output(clear,GPIO.LOW)
			time.sleep(speedShifts)
			GPIO.output(clear,GPIO.HIGH)
			time.sleep(speedShifts)
			print(output)
		if (aux == 'a'):
			if (auxclockP == False):
				print('clock set to automatic')
				auxclockP = True
				clockP.start()
		if (aux == 'd'):
			demoMode()
		if (aux == 'f'):
			flickerMode()
		if (aux == ','):
			setLatch(1)
		if (aux == '.'):
			setLatch(0)
		if (aux == 'w'):
			testWave()
	shift(0)
	GPIO.output(clear,GPIO.LOW)
	time.sleep(speedShifts)
	GPIO.output(clear,GPIO.HIGH)
	time.sleep(speedShifts)
			
	GPIO.output(clock,GPIO.HIGH)
			
	GPIO.cleanup()



if __name__ == "__main__":
	main()