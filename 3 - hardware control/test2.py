import hfmHardware


def main():

	outvrb = 11
	clock = 13
	clear = 15
	latch = 16

	speedShifts = 0.00005

	auxgpio = hfmHardware.hfmGpioModule(outvrb, clock, clear, latch, speedShifts, 16)

	aux = ''
	while(aux != 'q'):	
		aux = input('Enter your input:') 
		
		if( aux == 'h' ):
			auxgpio.inputNPulse(1)
			print(auxgpio.output)
		if( aux == 'c' ):
			auxgpio.clearOutput()
			print(auxgpio.output)
		if( aux == 'l' ):
			auxgpio.inputNPulse(0)
			print(auxgpio.output)
		if( aux == 'n'):
			auxInt = int(input('Enter a number:'))
			print(auxInt)
			auxgpio.setToInt(auxInt)
			print(auxgpio.output)
			
			
if __name__ == "__main__":
	main()