import RPi.GPIO as GPIO
import time
from multiprocessing import Process
import _thread

class hfmGpioModule:
	
	output = []
	
	def __init__(self, outvrbGpioIn, clockGpioIn, clearGpioIn, latchGpioIn, speedShiftsIn, numHapOutIn): 
		self.outvrbGpio = outvrbGpioIn
		self.clockGpio = clockGpioIn
		self.clearGpio = clearGpioIn
		self.latchGpio = latchGpioIn
		self.speedShifts = speedShiftsIn
		self.numHapOut = numHapOutIn
		self.output = [0]*self.numHapOut
		
		GPIO.setmode(GPIO.BOARD) 
		GPIO.setup(self.outvrbGpio, GPIO.OUT, initial=GPIO.LOW)
		GPIO.setup(self.clockGpio, GPIO.OUT, initial=GPIO.LOW)
		GPIO.setup(self.clearGpio, GPIO.OUT, initial=GPIO.HIGH)
		GPIO.setup(self.latchGpio, GPIO.OUT, initial=GPIO.HIGH)
		
		GPIO.output(self.clearGpio,GPIO.LOW)
		time.sleep(self.speedShifts)
		GPIO.output(self.clearGpio,GPIO.HIGH)
		time.sleep(self.speedShifts)
		
	def __del__(self):		
		GPIO.cleanup()
		
	def shift(self, input):
		if(input == 1):
			GPIO.output(self.outvrbGpio,GPIO.HIGH)
		elif(input == 0):
			GPIO.output(self.outvrbGpio,GPIO.LOW)
		else:
			return
		
	def setLatch(self, input):
		if(input == 1):
			GPIO.output(self.latchGpio,GPIO.HIGH)
			#print('latch on')
		elif(input == 0):
			GPIO.output(self.latchGpio,GPIO.LOW)
			#print('latch off')
		else:
			return
		
	def pulseClock(self):
		#print ('pulse')
		GPIO.output(self.clockGpio,GPIO.HIGH)
		time.sleep(self.speedShifts)
		GPIO.output(self.clockGpio,GPIO.LOW)
		time.sleep(self.speedShifts)
		
	def setToOutput(self):
		self.setLatch(0)
		for i in range(self.numHapOut,0, -1):
			self.shift(self.output[i-1])
			self.pulseClock()
		self.setLatch(1)
	
	def clearOutput(self):
		self.output = [0]*self.numHapOut
		GPIO.output(self.clearGpio,GPIO.LOW)
		time.sleep(self.speedShifts)
		GPIO.output(self.clearGpio,GPIO.HIGH)
		time.sleep(self.speedShifts)
	
	def inputNPulse(self, input):
		self.shift(input)
		self.pulseClock()
		self.output.pop(len(self.output)-1)
		self.output.insert(0,input)
	
	def setToInt(self, int):
		self.setLatch(0)
		for i in range (self.numHapOut):
			self.shift(int&1)
			self.pulseClock()
			self.output.pop(len(self.output)-1)
			self.output.insert(0,int&1)
			int >>= 1
		self.setLatch(1)
	