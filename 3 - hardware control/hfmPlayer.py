import hfmHardware
import hfmEditor
import argparse
import sys
import time

def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('File_To_Open',type=str, help = 'Name of the file with extension to open')
	args = parser.parse_args()
	
	outChn0 = 11
	clock = 13
	clear = 15
	latch = 16

	speedShifts = 0.00005
	
	channel0 = hfmHardware.hfmGpioModule(outChn0, clock, clear, latch, speedShifts, 16)
	
	hfmObject = hfmEditor.HFMFile()
	
	hfmObject.initFromFile(args.File_To_Open)
	
	print (hfmObject.lenghtOfFile)
	print (hfmObject.numChannels)
	
	input("file loaded press enter to start")
	
	startTime = time.time()
	
	for i in range (hfmObject.dataSize//hfmObject.numChannels):
		auxTime = time.time()
		#print(hfmObject.dataArray[0][i])
		channel0.setToInt(hfmObject.dataArray[0][i])
		loopTime = 1/hfmObject.freq
		while((time.time() - auxTime) < loopTime):
			pass
	
	endTime = time.time() - startTime
	
	print ("end time: ",endTime)
	
	print ("music lenght: ", hfmObject.lenghtOfFile)

if __name__ == "__main__":
	main()