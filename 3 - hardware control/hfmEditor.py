import sys
import struct

class HFMFile:
		
	dataArray = []
	
	def __init__(self, fileToOpen = None):
		if fileToOpen is not None:
			self.initFromFile(fileToOpen)
		else:
			self.initialized = False
			self.numChannels = 0 # numero de channels presente
			self.freq = 0.0 # frequencia em hz que os outputs mudam
			self.hapOutNum = 0 # numero de outputs em cada lado
			self.dataSize = 0 #length of dataArray
			self.lenghtOfFile = 0.0
			
	def dumpInfo(self):
		print('Initialize flag: ',self.initialized)
		print('num of channels: ',self.numChannels)
		print('frequency(hz): ',self.freq)
		print('num of haptic outputs: ',self.hapOutNum)
		print('Data size: ',self.dataSize)
		print('Lenght Of File: ',self.lenghtOfFile)
		#print('Data array: ',self.dataArray)
		
	def populateData(self, numChannelsIn, freqIn, hapOutNumIn, dataSizeIn, dataArrayIn):
		
		self.numChannels = numChannelsIn
		self.freq = freqIn
		self.hapOutNum = hapOutNumIn
		self.dataArray = dataArrayIn
		self.dataSize = dataSizeIn
		self.lenghtOfFile = (self.dataSize * 1/self.freq) / self.numChannels
		self.initialized = True
		
			
	def initFromFile(self, fileToOpen):
		with open(fileToOpen, "rb") as f:
			if (f.read(4) != b'HFFM'):
				raise Exception('Error: file is not HFM format')
			
			if (f.read(4) != b'meta'):
				raise Exception('Error: meta subchunk not present')
				
			self.numChannels = int.from_bytes(f.read(2), byteorder = 'big', signed = False)
			
			for i in range(self.numChannels):
				self.dataArray.append([])
			
			auxRefByt = f.read(8)
			
			self.freq = struct.unpack('d', auxRefByt)[0]
			
			self.hapOutNum = int.from_bytes(f.read(2), byteorder = 'big', signed = False)
					
			if (f.read(4) != b'data'):
				raise Exception('Error: data subchunk not present')

			self.dataSize = int.from_bytes(f.read(4), byteorder = 'big', signed = False)

			self.lenghtOfFile = (self.dataSize * 1/self.freq) / self.numChannels
			
			auxByte = self.hapOutNum//8
			if(self.hapOutNum%8 > 0):
				auxByte += 1
			
			for i in range (self.dataSize):
					aux = self.dataSize//self.numChannels
					self.dataArray[i//aux].append(int.from_bytes(f.read(auxByte), byteorder = 'big', signed = False))
		
		f.close() #não necessário usando with, mas sempre boto :/
		
		self.initialized = True
	
	def saveToFile(self, fileToSave):
		
		if(self.initialized == True):
		
			with open(fileToSave, "wb+") as f:
				f.write(b'HFFM')
				
				f.write(b'meta')
				
				f.write((self.numChannels).to_bytes(2, byteorder='big', signed = False))
				
				auxRefByt = struct.pack('d', self.freq)
				
				f.write(auxRefByt)
				
				f.write((self.hapOutNum).to_bytes(2, byteorder='big', signed = False))
				
				f.write(b'data')
				
				f.write((self.dataSize).to_bytes(4, byteorder='big', signed = False))
				
				auxByte = self.hapOutNum//8
				if(self.hapOutNum%8 > 0):
					auxByte += 1
				
				for i in range(self.numChannels):
					for j in range (self.dataSize//self.numChannels):
						f.write((self.dataArray[i][j]).to_bytes(auxByte, byteorder='big', signed = False))
					
				
			f.close() #não necessário usando with, mas sempre boto :/
		
		else:
			raise Exception('Error: tried to save non initialized hfm file')