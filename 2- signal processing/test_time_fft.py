import argparse
import sys
import wavEditor
import numpy as np
from timeit import default_timer as timer
import matplotlib.pyplot as plt


def main():

	testarray = np.array(np.random.random(2**17), dtype = complex)
		
	start = timer()
	aux = wavEditor.fft(testarray)
	end = timer()
	print('my timer', end - start)
	start = timer()
	aux21 = np.fft.fft(testarray)
	aux2 = 10* np.log10(np.sqrt(aux21.real*aux21.real+aux21.imag*aux21.imag))
	end = timer()
	print('np timer', end - start)

	print("\n all close test ",np.allclose(aux2,aux))

	
if __name__ == "__main__":
	main()