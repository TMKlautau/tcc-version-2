import argparse
import sys
import wavEditor
import numpy as np
import matplotlib.pyplot as plt


def main():

	#seta as variaveis de linha de comando, atualmente somente pega o arquivo wav para abrir
	parser = argparse.ArgumentParser()
	parser.add_argument('File_To_Open',type=str, help = 'Name of the file with extension to open')
	args = parser.parse_args()

	wavFile = wavEditor.WavFile(args.File_To_Open)
	spectrogram = wavEditor.Spectrogram(wavFile)
	
	spectrogram.saveToHFM(16)
	
	for i in range(spectrogram.wavFile.numChannels):
		auxTitle = 'spectogram channel ' + str(i)
		print('plotando grafico do spectogram do channel ', i, ':')
		auxfig = plt.figure()
		plt.pcolormesh(spectrogram.timeArray, spectrogram.frequencyArray ,spectrogram.spectrogramMatrix[:,:,i])
		plt.ylabel('Frequency - Hz')
		plt.xlabel('Time - seconds')
		plt.colorbar().set_label('Amplitude - dB', rotation=270)
		plt.title(auxTitle)
		figureName = args.File_To_Open + '-channel-' + str(i) + '.png'
		auxfig.savefig(figureName)
		plt.show()
		
	for i in range(spectrogram.wavFile.numChannels):
		auxTitle = 'hfm channel ' + str(i)
		print('plotando grafico do hfm do channel ', i, ':')
		auxfig = plt.figure()
		plt.pcolormesh(spectrogram.timeArray, range(17) ,spectrogram.hfmMatrix[:,:,i])
		plt.ylabel('haptic output')
		plt.xlabel('Time - seconds')
		#plt.colorbar().set_label('value', rotation=270)
		plt.title(auxTitle)
		figureName = args.File_To_Open + '-hfm-channel-' + str(i) + '.png'
		auxfig.savefig(figureName)
		plt.show()
	
	
	
	
if __name__ == "__main__":
	main()
