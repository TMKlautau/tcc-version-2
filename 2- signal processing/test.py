import numpy as np
import hfmEditor as hfm

def main():

	aux = hfm.HFMFile()
	
	aux2 = hfm.HFMFile()

	numChannels = 1
	
	freq = 21.537297668282942
	
	hapOutNum = 16
	
	dataArray = [range(0,20), range(20,40), range(40,60)]
	
	dataSize = len(dataArray[0]) * numChannels
	
	aux.populateData(numChannels, freq, hapOutNum, dataSize, dataArray)
	
	print ('aux')
	aux.dumpInfo()
	
	aux.saveToFile("test.hfm")
	
	aux2.initFromFile("hfmFiles\Crazy.hfm")
	
	print ('aux2')
	aux2.dumpInfo()
	
	aux2.saveToFile("test2.hfm")
		
if __name__ == "__main__":
	main()
