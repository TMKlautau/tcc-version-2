import hfmHardware
import hfmEditor
import argparse
import sys
import time

class HFMPlayer:

	outChannelsObjects = []

	def __init__(self, outChanelsIn, clockIn, clearIn, latchIn, speedShiftsIn, numHapOutIn):
		self.hfmObject = hfmEditor.HFMFile()
		self.outChanels = outChanelsIn
		self.clock = clockIn
		self.clear = clearIn
		self.latch = latchIn
		self.speedShifts = speedShiftsIn
		self.numHapOut = numHapOutIn
		self.fileLoaded = False
		
	def loadFile(self, fileToOpen):
		self.hfmObject.initFromFile(fileToOpen)
		
		for i in range(self.hfmObject.numChannels):
			self.outChannelsObjects.append(hfmHardware.hfmGpioModule(self.outChanels[i], self.clock, self.clear, self.latch, self.speedShifts, self.numHapOut))
			
		self.fileLoaded = True
		
	def setToIntAllChannels(self, values): #arrumar isso depois, não eh a melhor solução
		self.outChannelsObjects[0].setLatch(0)
		for i in range (self.numHapOut):
			for j in range(self.hfmObject.numChannels):
				self.outChannelsObjects[j].shift(values[j]&1)
				self.outChannelsObjects[0].pulseClock()
				values[j] >>= 1
		self.outChannelsObjects[0].setLatch(1)
			
	def startPlaybackLoadedFile(self):
		if (self.fileLoaded == False):
			raise Exception('Tried to start playback without loading file')
		else:
			for i in range (self.hfmObject.dataSize//self.hfmObject.numChannels):
				auxTime = time.time()
				
				auxIntArray = []
				
				for j in range(self.hfmObject.numChannels):
					auxIntArray.append(self.hfmObject.dataArray[j][i])
					#self.outChannelsObjects[j].setToInt(self.hfmObject.dataArray[j][i]) # arrumar aqui, clock de ambos são o mesmo então esta dando duplo clock
				
				self.setToIntAllChannels(auxIntArray)
				
				loopTime = 1/self.hfmObject.freq
				while((time.time() - auxTime) < loopTime):
					pass
	
	def testMode(self):
		print("inicialized on test mode")
		
		print("outvrb = 11")
		print("clock = 13")
		print("clear = 15")
		print("latch = 16")
		print("speedShifts = 0.000005")
		print("numOutputs = 16")
		
		auxgpio = hfmHardware.hfmGpioModule(11, 13, 15, 16, 0.000005, 16)

		aux = ''
		while(aux != 'q'):	
			aux = input('Enter your input:') 
			
			if( aux == 'h' ):
				auxgpio.inputNPulse(1)
				print(auxgpio.output)
			if( aux == 'c' ):
				auxgpio.clearOutput()
				print(auxgpio.output)
			if( aux == 'l' ):
				auxgpio.inputNPulse(0)
				print(auxgpio.output)
			if( aux == 'n'):
				auxInt = int(input('Enter a number:'))
				print(auxInt)
				auxgpio.setToInt(auxInt)
				print(auxgpio.output)
		
		auxgpio.cleanUp()
			
	def __del__(self):
		if(self.fileLoaded == True):
			self.outChannelsObjects[0].cleanUp()
		
def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-f','--File_To_Open',type=str, help = 'Name of the file with extension to open')
	parser.add_argument('-t','--Test_Mode', help = 'Start on test mode', action="store_true")
	args = parser.parse_args()
	
	outChanels = [11,18]
	clock = 13
	clear = 15
	latch = 16

	speedShifts = 0.000005

	player = HFMPlayer(outChanels, clock, clear, latch, speedShifts, 16)

	if(args.Test_Mode):
		player.testMode()
	elif(args.File_To_Open == None):
		
		print("use the -f argument to open a file, use -h to get help")
	
	else:
		player.loadFile(args.File_To_Open)
		
		print("To play this file you will need ", (player.hfmObject.numChannels*player.numHapOut), " outputs")
		
		for i in range(player.hfmObject.numChannels):
			print("Please connect ", player.numHapOut, "outputs to GPIO number ", player.outChanels[i])
			
		print("Please connect all outputs modules clocks to GPIO number ", player.clock)
		
		print("Please connect all outputs modules clears to GPIO number ", player.clear)
		
		print("Please connect all outputs modules latches to GPIO number ", player.latch)
		
		input("file loaded press enter to start")
		
		print("Playing file")
		
		startTime = time.time()
		
		player.startPlaybackLoadedFile()
		
		endTime = time.time() - startTime
		
		print ("end time: ",endTime)
		
		print ("music lenght: ", player.hfmObject.lenghtOfFile)

if __name__ == "__main__":
	main()