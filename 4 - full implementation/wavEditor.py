import argparse
import sys
import numpy as np
from timeit import default_timer as timer
import progressBar
import hfmEditor as hfm
import struct
import os


class WavFile:

	#python não precisa de inicialização, coloco por costume
	
	#int chunkSize = 0
	#int fmtChunkSize = 0
	#int audioFormat = 0
	#int numChannels = 0
	#int sampleRate = 0
	#int byteRate = 0
	#int blockAlign = 0
	#int bitsPerSample = 0
	#int dataChunkSize = 0
	#int array dataArrays
	
	dataArrays = []
	
	def __init__(self, fileToOpen):
		self.fileName = fileToOpen
		#verifica endianess do meio
		byteOrder = sys.byteorder
		#http://soundfile.sapp.org/doc/WaveFormat/
		
		print ('Reading data from file: ',self.fileName)
		
		with open(fileToOpen, "rb") as f:
			while True:
				if (f.read(4) != b'RIFF'):
					print('Error: file is not wav format')
					break
					
				self.chunkSize = int.from_bytes(f.read(4), byteorder = byteOrder)
				
				if (f.read(4) != b'WAVE'):
					print('Error: file is not wav format')
					break
				
				#começo do ftm subchunk
				
				if (f.read(4) != b'fmt '):
					print('Error: fmt subchunk not present')
					break

				self.fmtChunkSize = int.from_bytes(f.read(4), byteorder = byteOrder)
				
				self.audioFormat = int.from_bytes(f.read(2), byteorder = byteOrder)
				if (self.audioFormat != 1):
					print('Error: PCM not equal 1, file is compressed')
					break

				self.numChannels = int.from_bytes(f.read(2), byteorder = byteOrder)
						
				self.sampleRate = int.from_bytes(f.read(4), byteorder = byteOrder)
				
				self.byteRate = int.from_bytes(f.read(4), byteorder = byteOrder)
				
				self.blockAlign = int.from_bytes(f.read(2), byteorder = byteOrder)
				
				self.bitsPerSample = int.from_bytes(f.read(2), byteorder = byteOrder)
				
				#começo do data subchunk
				
				lookingForDataSubchunk = True
				while lookingForDataSubchunk :
					auxChunkName = f.read(4)
					if (auxChunkName != b'data'):
						auxSubchunkSize = int.from_bytes(f.read(4), byteorder = byteOrder)
						f.read(auxSubchunkSize)
					elif (auxChunkName == ''):
						raise Exception('Error: data subchunk not present')
						break
					else:
						lookingForDataSubchunk = False
				
				self.dataChunkSize = int.from_bytes(f.read(4), byteorder = byteOrder)
								
				for i in range(self.numChannels):
					self.dataArrays.append([])
					
				aux = self.bitsPerSample//8
				dataChunkBuffer = []
				for i in range(self.dataChunkSize//aux):
					dataChunkBuffer.append(int.from_bytes(f.read(aux),byteorder = byteOrder, signed=True))
				
				print ('Processing data:')
				
				
				for i in range(len(dataChunkBuffer)):
					self.dataArrays[i%self.numChannels].append(dataChunkBuffer[i])
					#print ( "channel: ", i%self.numChannels , "  --  amostra: ", i, " int value -- " , self.dataArrays[i%self.numChannels][(i//self.numChannels) + i%self.numChannels])
					
				break

		f.close() #não necessário usando with, mas sempre boto :/
		


		
			
class Spectrogram:

	#

	def __init__(self, wavFileToCalculate: WavFile, refreshRateCeilingIn = 25):
		self.refreshRate = refreshRateCeilingIn
		self.wavFile = wavFileToCalculate
		self.numberOfSamples = (self.wavFile.dataChunkSize//(self.wavFile.bitsPerSample//8))//self.wavFile.numChannels
		#lenght in seconds
		self.lenghOfFile = self.wavFile.dataChunkSize/(self.wavFile.sampleRate * self.wavFile.blockAlign)

	
		self.lenghtOfSegment = self.wavFile.sampleRate//self.refreshRate #define o floor do tamanho do segment divindo o sample rate pela quantidade maxima de vezes que os sensores irão dar refresh em 1 segundo
		
		self.lenghtOfSegment -= 1									#https://graphics.stanford.edu/~seander/bithacks.html
		self.lenghtOfSegment |= self.lenghtOfSegment >> 1
		self.lenghtOfSegment |= self.lenghtOfSegment >> 2
		self.lenghtOfSegment |= self.lenghtOfSegment >> 4
		self.lenghtOfSegment |= self.lenghtOfSegment >> 8
		self.lenghtOfSegment |= self.lenghtOfSegment >> 16
		self.lenghtOfSegment += 1
		
		self.numberOfSegments = self.numberOfSamples//self.lenghtOfSegment
		
		if ((self.numberOfSamples % self.lenghtOfSegment)!= 0):
			self.numberOfSegments += 1
		
		self.spectrogramMatrix = np.zeros(shape = (self.lenghtOfSegment,self.numberOfSegments, self.wavFile.numChannels))
		
		for j in range(self.wavFile.numChannels):
			print('calculando spectogram do channel: ',j)
			if ((self.numberOfSamples % self.lenghtOfSegment)!= 0):
				for i in range(self.numberOfSegments-1):
					self.spectrogramMatrix[:,i,j] = fft(self.wavFile.dataArrays[j][i*self.lenghtOfSegment:(i+1)*self.lenghtOfSegment])				
					progressBar.progress(i, self.numberOfSegments-2,status='calculando spectogram')
				
				self.spectrogramMatrix[:self.lenghtOfSegment - (self.numberOfSamples % self.lenghtOfSegment),self.numberOfSegments-1,j] = self.wavFile.dataArrays[0][-(self.lenghtOfSegment - (self.numberOfSamples % self.lenghtOfSegment)):]
				self.spectrogramMatrix[:,self.numberOfSegments-1,j] = fft(self.spectrogramMatrix[:,self.numberOfSegments-1,j])
				
			else:
				for i in range(self.numberOfSegments):
					self.spectrogramMatrix[:,i,j] = fft(self.wavFile.dataArrays[j][i*self.lenghtOfSegment:(i+1)*self.lenghtOfSegment])
					progressBar.progress(i, self.numberOfSegments-1,status='calculando')

			print('')
			print('concluido spectogram do channel: ',j)
			
		self.spectrogramMatrix = self.spectrogramMatrix[:(-self.lenghtOfSegment//2),:,:]
		
		self.timeArray = np.empty(self.numberOfSegments)
		
		for i in range(self.numberOfSegments):
			self.timeArray[i] =  i * self.lenghOfFile/self.numberOfSegments
			
		self.refreshRate = 1/(self.timeArray[1] - self.timeArray[0])
		
		self.frequencyArray = np.empty(self.lenghtOfSegment//2)
		
		for i in range(self.lenghtOfSegment//2):
			self.frequencyArray[i] =  i * self.wavFile.sampleRate/self.lenghtOfSegment

			
	def saveToHFM(self, hapOutNumIn):
		auxHFM = hfm.HFMFile()
		
		auxNumChannels = self.wavFile.numChannels
		
		auxFreq = self.refreshRate
		
		auxHapOutNum = hapOutNumIn
		
		auxDataSize = self.numberOfSegments * self.wavFile.numChannels
		
		auxDataArray = [] #arrumar o data array
		
		for i in range(auxNumChannels):
			auxDataArray.append([])
		
		self.cutToFreq(8000)
		
		indexRangesFreqs = len(self.frequencyArray)//auxHapOutNum
		
		self.hfmMatrix = np.zeros(shape = (auxHapOutNum,self.numberOfSegments, self.wavFile.numChannels))
		
		for c in range (self.wavFile.numChannels):
			for i in range (self.numberOfSegments):
				#auxElementDataArray = 0
				bufferElementDataArray = 0
				for j in range (auxHapOutNum):
					auxDb = 0.0
					for k in range (indexRangesFreqs):
						auxDb += self.spectrogramMatrix[(j+1)*k, i, c]
					auxDb = auxDb/indexRangesFreqs
					
					if(auxDb > 50):
						self.hfmMatrix[j, i, c] = 1
						bufferElementDataArray += 1
					bufferElementDataArray <<= 1
					
				bufferElementDataArray = bufferElementDataArray>>1
				
				#while(bufferElementDataArray):
				#	auxElementDataArray = (auxElementDataArray << 1) + (bufferElementDataArray & 1)
				#	bufferElementDataArray >>= 1
				
				#print (self.hfmMatrix[:, i, c])
				#print(auxElementDataArray)
				#auxDataArray[c].append(auxElementDataArray)
				auxDataArray[c].append(bufferElementDataArray)
		auxHFM.populateData(auxNumChannels, auxFreq, auxHapOutNum, auxDataSize, auxDataArray)
		
		#auxHFM.dumpInfo()
		
		auxFileNameWExt=os.path.basename(self.wavFile.fileName)
		auxFileName = os.path.splitext(auxFileNameWExt)[0]
		auxHFM.saveToFile(os.path.join("hfmFiles", auxFileName + ".hfm"))
		print("hfm file created")

	def cutToFreq(self, freqToCut):
		indexToCut = np.searchsorted(self.frequencyArray, freqToCut, side='right')
		
		self.frequencyArray = self.frequencyArray[:indexToCut]
		self.spectrogramMatrix = self.spectrogramMatrix[:indexToCut,:,:]
		
#implementação da fft
def rec_fft(passedArray):
	n = len(passedArray) #len é de compexidade O(1) então não era necessário esta associação a variavel, esta é feita para melhor leitura do código

	factor = np.exp((-2j*np.pi)/n)

	if (n<1): 
		raise Exception('fft recursivo recebeu um array vazio, não deveria ocorrer')
		
	if (n>1):
		passedArray = np.concatenate([rec_fft(passedArray[::2]), rec_fft(passedArray[1::2])])
		for i in range (n//2):
			aux = passedArray[i]
			passedArray[i] = aux+(factor**i)*passedArray[i+n//2]
			passedArray[i+n//2] = aux-(factor**i)*passedArray[i+n//2]
				
	return (passedArray)

def fft(passedArray):
	
	n = len(passedArray)
			
	if n & (n - 1) != 0: #teste para ver se N é multiplo de 2
		raise Exception('error ao passar array para calculo de fft por coley tukey, obrigatoriamente deve ser um multiplo de 2, verificar divisão de setores')
	
	array = np.array(passedArray, dtype = complex)
	
	array = rec_fft(array)
	
	magArray = np.sqrt(array.real*array.real+array.imag*array.imag) #magnitude do fft do array dado,

	for i in range(len(magArray)): 							# conversão do magnitude para decibels, numpy acusa warning se algum valor do vetor iguala a 0 , como magArray é somente valores positivos não precisamos preocupar com valores negativos
		if (magArray[i] > 0):			
			magArray[i] = 10* np.log10(magArray[i]) 		# https://dsp.stackexchange.com/questions/1262/creating-a-spectrogram , por isso se calcula a conversão elemento por elemento mesmo assim sendo muito mais lento
	
	return magArray
	

