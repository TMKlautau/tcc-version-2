import argparse
import sys
import wavEditor
import numpy as np
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import struct


def main():

	#seta as variaveis de linha de comando, atualmente somente pega o arquivo wav para abrir
	parser = argparse.ArgumentParser()
	parser.add_argument('-f','--File_To_Open',type=str, help = 'Name of the file with extension to open')
	parser.add_argument('-o','--Num_Outputs',type=int, help = 'Number of haptics outputs present on the hardware where the hfm file will be executed')
	parser.add_argument('-r','--Refresh_Rate_Ceiling',type=int, help = 'Ceiling refresh rate on HFM file')
	parser.add_argument('-g','--ShowGraph', help = 'Show Graph after wav file load and hfm file creation', action="store_true")
	parser.add_argument('-i','--DumpInfo', help = 'Show file info on screen', action="store_true")
	args = parser.parse_args()

	if(args.File_To_Open == None):
		args.File_To_Open = input("Insert path to Wav file:")
		
	if(args.Num_Outputs == None):
		args.Num_Outputs = int(input("Insert number of haptics outputs present on the hardware where the hfm file will be executed:"))
	
	if(args.Refresh_Rate_Ceiling == None):
		args.Refresh_Rate_Ceiling = int(input("Insert Ceiling refresh rate on HFM file:"))
	
	wavTimeStart = timer()
	wavFile = wavEditor.WavFile(args.File_To_Open)
	wavTimeEnd = timer()
	
	specTimeStart = timer()
	spectrogram = wavEditor.Spectrogram(wavFile, args.Refresh_Rate_Ceiling)
	specTimeEnd = timer()
	
	spectrogram.saveToHFM(args.Num_Outputs)
	
	if(args.DumpInfo):
			
		print('leitura e processamento do arquivo wav concluido, tempo em segundos :', wavTimeEnd - wavTimeStart)		
		print('calculo do spectogram concluido, tempo em segundos :', specTimeEnd - specTimeStart)
		print ('ChunkSize:',wavFile.chunkSize)
		print ('fmtChunkSize:',wavFile.fmtChunkSize)
		print ('AudioFormat:',wavFile.audioFormat)
		print ('Number of channels:',wavFile.numChannels)
		print ('Sample Rate:',wavFile.sampleRate)
		print ('Byte Rate:',wavFile.byteRate)
		print ('Block Align:',wavFile.blockAlign)
		print ('Bits per Sample:',wavFile.bitsPerSample)
		print ('dataChunkSize:',wavFile.dataChunkSize)
	
		print('number of samples', spectrogram.numberOfSamples)
		print('lenght of file in seconds', spectrogram.lenghOfFile)
		
		print('ajustado length of segment: ', spectrogram.lenghtOfSegment)
		print('length of spectogram matrix = ', spectrogram.spectrogramMatrix.shape)
	
		print ("refreshRate = ",spectrogram.refreshRate)
		print ("numberOfSegments = ",spectrogram.numberOfSegments)
		auxRefByt = struct.pack('d', spectrogram.refreshRate)
		print ("refresh rate in bytes = ", auxRefByt)
		print ("refresh rate in bytes lenght = ", len(auxRefByt))
		auxRefBack = struct.unpack('d', auxRefByt)
		print ("refresh rate converted back = ", auxRefBack)
		print ("lenght of file = ",(1/spectrogram.refreshRate)*spectrogram.numberOfSegments)
	
	if(args.ShowGraph):

		for i in range(spectrogram.wavFile.numChannels):
			auxTitle = 'spectogram channel ' + str(i)
			print('plotando grafico do spectogram do channel ', i, ':')
			auxfig = plt.figure()
			plt.pcolormesh(spectrogram.timeArray, spectrogram.frequencyArray ,spectrogram.spectrogramMatrix[:,:,i])
			plt.ylabel('Frequency - Hz')
			plt.xlabel('Time - seconds')
			plt.colorbar().set_label('Amplitude - dB', rotation=270)
			plt.title(auxTitle)
			figureName = args.File_To_Open + '-channel-' + str(i) + '.png'
		#	auxfig.savefig(figureName)
			plt.show()
			
		for i in range(spectrogram.wavFile.numChannels):
			auxTitle = 'hfm channel ' + str(i)
			print('plotando grafico do hfm do channel ', i, ':')
			auxfig = plt.figure()
			plt.pcolormesh(spectrogram.timeArray, range(17) ,spectrogram.hfmMatrix[:,:,i])
			plt.ylabel('haptic output')
			plt.xlabel('Time - seconds')
			#plt.colorbar().set_label('value', rotation=270)
			plt.title(auxTitle)
			figureName = args.File_To_Open + '-hfm-channel-' + str(i) + '.png'
		#	auxfig.savefig(figureName)
			plt.show()
	
	
	
	
if __name__ == "__main__":
	main()
